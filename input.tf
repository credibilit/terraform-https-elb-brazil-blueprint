variable "account" {
  description = "The AWS account number ID"
}

variable "name" {
  description = "A name for the ELB and related resources"
}

variable "instance_security_group" {
  description = "The security group for the instances behind the ELB"
}

variable "subnets" {
  type = "list"
  description = "A list of subnet IDs to attach to the ELB"
}

variable "elb_https_port" {
  description = "The ELB HTTPS port to listen on for the load balancer"
  default = "443"
}

variable "elb_http_port" {
  description = "The ELB HTTP port to listen on for the load balancer"
  default = "80"
}

variable "instance_https_port" {
  description = "The HTTPS port on the instance to route to"
  default = "443"
}

variable "instance_https_protocol" {
  description = "The HTTPS protocol on the instance to route to"
  default = "https"
}

variable "instance_http_port" {
  description = "The HTTP port on the instance to route to"
  default = "80"
}

variable "s3_bucket_force_destroy" {
  description = "If the bucket must destroy all objects inside the bucket before delete the bucket (default: false)"
  default = false
}

variable "lifecycle_rule_days_sia" {
  description = "How many days the objects will stay in default storage area before go to SIA. (default: 30 day)"
  default = "30"
}

variable "lifecycle_rule_expiration" {
  description = "In how many days the objects will be deleted. (default: 90 days)"
  default = "90"
}

variable "https_instance_protocol" {
  description = "The protocol to use to the instance. Valid values are HTTP, HTTPS"
  default = "https"
}

variable "access_logs_interval" {
  description = "The publishing interval in minutes. Default: 5 minutes"
  default = "5"
}

variable "internal" {
  description = "If true, ELB will be an internal ELB"
  default = "true"
}

variable "ssl_certificate_arn" {
  description = "The ARN of an SSL certificate you have uploaded to AWS IAM"
}

variable "health_check_healthy_threshold" {
  description = "The number of checks before the instance is declared healthy"
  default = "2"
}

variable "health_check_unhealthy_threshold" {
  description = "The number of checks before the instance is declared unhealthy"
  default = "2"
}

variable "health_check_target" {
  description = "The target of the check. Valid pattern is PROTOCOL:PORT/PATH"
  default = "HTTPS:443/"
}

variable "health_check_interval" {
  description = "The interval between checks"
  default = "10"
}

variable "health_check_timeout" {
  description = "The length of time before the check times out"
  default = "5"
}

variable "cross_zone_load_balancing" {
  description = "Enable cross-zone load balancing. Default: true"
  default = "true"
}

variable "idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle. Default: 60s"
  default = "60"
}

variable "connection_draining" {
  description = "Boolean to enable connection draining. Default: true"
  default = "true"
}

variable "connection_draining_timeout" {
  description = "The time in seconds to allow for connections to drain. Default: 60s"
  default = "60"
}

variable "tags" {
  type = "map"
  description = "A mapping of tags to assign to the resource"
  default = {}
}
