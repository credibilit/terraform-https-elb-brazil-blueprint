Generic ELB Terraform Module
============================

# Use

To create an ELB with this module, you need to insert the following peace of code on your own modules:

```
// Call the module
module "elb" {
  source  = "git::ssh://git@bitbucket.org/credibilit/terraform-https-elb-brazil-blueprint.git?ref=<VERSION>"
  account = "${var.account}"

  name                    = "${var.name}"
  instance_security_group = "${aws_security_group.instance_http.id}"
  subnets                 = ["${module.vpc.public_subnets}"]
  s3_bucket_force_destroy = true
  internal                = false
  tags                    = "${map("Environment", "test")}"
  ssl_certificate_arn     = "${aws_iam_server_certificate.acme.arn}"
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.


## Input parameters

- `account`: The account number ID
- `name`: A name for the ELB and related resources
- `instance_security_group`: The security group for the instances behind the ELB
- `subnets`: A list of subnet IDs to attach to the ELB
- `elb_https_port`: The ELB HTTPS port to listen on for the load balancer
- `elb_http_port`: The ELB HTTP port to listen on for the load balancer
- `instance_https_port`: The HTTPS port on the instance to route to
- `instance_http_port`: The HTTP port on the instance to route to
- `s3_bucket_force_destroy`: If the bucket must destroy all objects inside the bucket before delete the bucket (default: false)
- `lifecycle_rule_days_sia`: How many days the objects will stay in default storage area before go to SIA. (default: 30 day)
- `lifecycle_rule_days_glacier`: How many days the objects will stay in SIA storeage area before go to Glacier. (default: 30 days)
- `lifecycle_rule_expiration`: In how many days the objects will be deleted. (default: 90 days)
- `https_instance_protocol`: The protocol to use to the instance. Valid values are HTTP, HTTPS
- `access_logs_interval`: The publishing interval in minutes. Default: 5 minutes
- `internal`: If true, ELB will be an internal ELB
- `ssl_certificate_arn`: The ARN of an SSL certificate you have uploaded to AWS IAM
- `health_check_healthy_threshold`: The number of checks before the instance is declared healthy
- `health_check_unhealthy_threshold`: The number of checks before the instance is declared unhealthy
- `health_check_target`: The target of the check. Valid pattern is PROTOCOL:PORT/PATH
- `health_check_interval`: The interval between checks
- `health_check_timeout`: The length of time before the check times out
- `cross_zone_load_balancing`: Enable cross-zone load balancing. Default: true
- `idle_timeout`: The time in seconds that the connection is allowed to be idle. Default: 60s
- `connection_draining`: Boolean to enable connection draining. Default: true
- `connection_draining_timeout`: The time in seconds to allow for connections to drain. Default: 60s
- `tags`: A mapping of tags to assign to the resource


## Output parameters

- `id`
- `name`
- `dns_name`
- `instances`
- `source_security_group_id`
- `zone_id`
- `log_bucket`
- `log_bucket_arn`
- `elb_security_group`
