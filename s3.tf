module "s3_bucket_access_log" {
  source  = "git::https://bitbucket.org/credibilit/terraform-s3-brazil-bucket-blueprint.git?ref=1.0.2"
  account = "${var.account}"

  bucket_name                 = "logs-${var.name}"
  force_destroy               = "${var.s3_bucket_force_destroy}"
  acl                         = "private"
  versioning_enabled          = false
  lifecycle_rule_enabled      = true
  lifecycle_rule_days_sia     = "${var.lifecycle_rule_days_sia}"
  lifecycle_rule_expiration   = "${var.lifecycle_rule_expiration}"
}

data "aws_elb_service_account" "elb_account" { }

data "aws_iam_policy_document" "s3_bucket_policy_body" {
  statement {
    sid       = "ELB log permission"
    actions   = ["s3:PutObject"]
    resources = ["${module.s3_bucket_access_log.arn}/AWSLogs/${var.account}/*"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_elb_service_account.elb_account.id}:root"]
    }
  }
}

resource "aws_s3_bucket_policy" "s3_bucket_policy" {
  bucket = "${module.s3_bucket_access_log.id}"
  policy = "${data.aws_iam_policy_document.s3_bucket_policy_body.json}"
}
