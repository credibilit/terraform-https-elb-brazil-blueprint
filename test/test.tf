 /*Unit test file for module
 * =========================
 * Declare the folowing variable in environment to execute this code:
 * - AWS_PROFILE
 *
 * or the folowing variables:
 * - AWS_ACCESS_KEY_ID
 * - AWS_SECRET_ACCESS_KEY
 * - AWS_DEFAULT_REGION
 * - AWS_SECURITY_TOKEN (if applicable)
 */

/* AWS account
 * Declare this on TF_VAR_account environment variable with your AWS account id
 * for the test account
 */
//variable account {}

/*// VPC
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  tags {
    Name = "test_elb"
  }
}

// internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags {
    Name = "igw"
  }
}

// prv subnets
resource "aws_subnet" "prv-a" {
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "us-east-1a"
  cidr_block        = "${cidrsubnet(aws_vpc.vpc.cidr_block, 8, 1)}"
}

resource "aws_subnet" "prv-b" {
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "us-east-1b"
  cidr_block        = "${cidrsubnet(aws_vpc.vpc.cidr_block, 8, 2)}"
}

// pub subnets
resource "aws_subnet" "pub-a" {
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "us-east-1a"
  cidr_block        = "${cidrsubnet(aws_vpc.vpc.cidr_block, 8, 11)}"
}

resource "aws_subnet" "pub-b" {
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "us-east-1b"
  cidr_block        = "${cidrsubnet(aws_vpc.vpc.cidr_block, 8, 12)}"
}

// nat gateway
resource "aws_eip" "nat-gw-a" {
  vpc = true
}

resource "aws_nat_gateway" "nat-gw-a" {
  allocation_id = "${aws_eip.nat-gw-a.id}"
  subnet_id     = "${aws_subnet.pub-a.id}"
}

// route tables
resource "aws_route_table" "rt-pub" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags {
    Name = "pub"
  }
}

resource "aws_route_table" "rt-prv" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat-gw-a.id}"
  }
  tags {
    Name = "prv"
  }
}

resource "aws_route_table_association" "rt-association-prv-a" {
  subnet_id      = "${aws_subnet.prv-a.id}"
  route_table_id = "${aws_route_table.rt-prv.id}"
}

resource "aws_route_table_association" "rt-association-prv-b" {
  subnet_id      = "${aws_subnet.prv-b.id}"
  route_table_id = "${aws_route_table.rt-prv.id}"
}

resource "aws_route_table_association" "rt-association-pub-a" {
  subnet_id      = "${aws_subnet.pub-a.id}"
  route_table_id = "${aws_route_table.rt-pub.id}"
}

resource "aws_route_table_association" "rt-association-pub-b" {
  subnet_id      = "${aws_subnet.pub-b.id}"
  route_table_id = "${aws_route_table.rt-pub.id}"
}

// security group rule
resource "aws_security_group_rule" "allow_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [
    "0.0.0.0/0"
  ]
  security_group_id = "${module.test_public_elb.elb_security_group_id}"
}

module "test_public_elb" {
  account                   = "${var.account}"
  source                    = "../"
  elb_name                  = "test-public-elb"
  s3_bucket_access_log_name = "test-public-elb-access-log"
  s3_bucket_force_destroy   = true
  subnets                   = [
    "${aws_subnet.pub-a.id}",
    "${aws_subnet.pub-b.id}"
  ]
  tags                      = {
    Name = "test-public-elb"
  }
}

resource "aws_iam_server_certificate" "test_cert_alt" {
  name             = "alt_test_cert"
  certificate_body = "${file("keys/acme.com.crt")}"
  private_key      = "${file("keys/acme.com.key")}"
}

module "test_secure_public_elb" {
  account                   = "${var.account}"
  source                    = "../"
  elb_name                  = "test-public-secure-elb"
  s3_bucket_access_log_name = "test-public-secure-elb-access-log"
  s3_bucket_force_destroy   = true
  subnets                   = [
    "${aws_subnet.pub-a.id}",
    "${aws_subnet.pub-b.id}"
  ]

  listener_lb_port            = "443"
  listener_lb_protocol        = "https"
  listener_ssl_certificate_id = "${aws_iam_server_certificate.test_cert_alt.arn}"

  tags                        = {
    Name = "test-public-secure-elb"
  }
}*/

/*
module "test_private_elb" {
  source  = "../"
}
*/
