resource "aws_elb" "elb" {
  name       = "${var.name}"
  depends_on = [
    "aws_s3_bucket_policy.s3_bucket_policy"
  ]
  access_logs {
    bucket        = "${module.s3_bucket_access_log.id}"
    bucket_prefix = ""
    interval      = "${var.access_logs_interval}"
  }

  security_groups = ["${aws_security_group.elb.id}"]
  subnets = ["${var.subnets}"]
  internal = "${var.internal}"

  listener {
    instance_port      = "${var.instance_http_port}"
    instance_protocol  = "http"
    lb_port            = "${var.elb_http_port}"
    lb_protocol        = "http"
  }

  listener {
    instance_port      = "${var.instance_https_port}"
    instance_protocol  = "${var.instance_https_protocol}"
    lb_port            = "${var.elb_https_port}"
    lb_protocol        = "https"
    ssl_certificate_id = "${var.ssl_certificate_arn}"
  }

  health_check {
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    target              = "${var.health_check_target}"
    interval            = "${var.health_check_interval}"
    timeout             = "${var.health_check_timeout}"
  }

  cross_zone_load_balancing   = "${var.cross_zone_load_balancing}"
  idle_timeout                = "${var.idle_timeout}"
  connection_draining         = "${var.connection_draining}"
  connection_draining_timeout = "${var.connection_draining_timeout}"
  tags                        = "${
    merge(
      map("Name", "${var.name}"),
      var.tags
    )
  }"
}
